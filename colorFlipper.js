
var colors = ["red", "blue", "green", "yellow", "purple", "gray", "orange", "pink"];

document.querySelector("button").addEventListener("click", function() {
  var randomNumber = Math.floor(Math.random() * 8);
  var randomColor = colors[randomNumber];
  document.querySelector("body").style.background = randomColor;
  $("button").fadeIn(100).fadeOut(100).fadeIn(100);
  document.querySelector("h1 strong").textContent = randomColor;
  document.querySelector("h1 strong").classList.add("cap")
});
